// this is the file containing the types that will be placed on the screen when the user clicks on an info icon
//

let popFac =()=>{
  let id =0
  let basePopOut = ()=> {
    let ob = {}
    ob.create = () => {
      // make a div
      // name it specifically
      // include a little x icon
      // image
      let holder = document.createElement("div")
      console.log("loading")
      holder.className="popoutHolder"
      ob.holder = holder
      let img = new Image()
      img.onload = ()=> {
        // include the on clicks
        img.addEventListener("click",()=> {
          console.log("bam!]")
          holder.remove()
        })
      }
      let divxholder = document.createElement("div")
      divxholder.id = "xholder"
      img.src = "./resources/brand_icons/ua-brand-icons/ua-brand-icons-image-files/SVG/x.svg"
      img.id = "exit"
      divxholder.append(img)
      holder.id = "holder"+id
      holder.append(divxholder)
      id+=1
      document.body.append(holder)
    }
    return ob
  }
  return basePopOut
}

let base = popFac()

let textPopout = ()=> {
  let ob = {}
  // compose base 
  ob.base = base()
  console.log(ob.base)
  // assume that the text is already formatted html taken from a file describing one of the icons
  ob.create = (text)=> {
    // create the basic 
    ob.base.create()
    let holder = ob.base.holder
    let textHolder = document.createElement("div")
    textHolder.id = "textholder"
    textHolder.innerHTML = text
    holder.append(textHolder)
    holder.style.overflow = "scroll"
    holder.style.height="75%"
    holder.style.left = `${window.innerWidth/2 - holder.getBoundingClientRect().width/2}px`
    holder.style.top = `${window.innerHeight/2 - holder.getBoundingClientRect().height/2}px`
    // style holder to be fully on page
    holder.style.position="absolute"

  }
  return ob
}
