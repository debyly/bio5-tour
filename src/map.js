

let svgDraw = (imagepath,roiIds) => {
  let ob = {}
  let svg = document.createElement("object")
  svg.height=window.innerHeight-100
  svg.width=window.innerWidth-100
  svg.setAttribute("type","image/svg+xml")
  svg.setAttribute("data",imagepath)
  // redirects, the order here is numerically 1-11 also
  svg.onload = ()=> {
    // attach the onhover
    for (let roiId of roiIds) {
      let ele = svg.contentDocument.getElementById(roiId)
      if (ele) {
      let col = ele.style.fill
      ele.onmouseenter=()=> {
        ele.style.setProperty("fill","#333")
        console.log("hi")
      }
      ele.onmouseleave = ()=> {
        ele.style.setProperty("fill",col)
      }
      ele.onclick =()=> {
        // nav to specific page
        let basicscene = basicScene(roiId)
        // create loading window
        basicscene.create()

      }
    }
    }
  }
  document.querySelector("#container").append(svg)
  ob.svg = svg
  return ob
}
// rooms of interest
// make this generic, so a list can be provided
let rooms = [
  "start-rect",
  "art-one",
  "art-two",
  "art-three",
  "art-four",
]
// change the background on clicks 
//
let floorNav = ()=> {
  let ob = {}
  
  ob.changeCol = (ele) => {
    // find prev, and remove the id from it 
    let prev = document.querySelector("#active")
    if (prev) {
      prev.id = ""
    }
    // assign ele the active id
    ele.id = "active"
  }
  // add listener to the floor tags
  Array(...document.querySelectorAll(".floor")).map(e => {
    e.addEventListener("click",()=> {
      ob.changeCol(e)
      // delete svg ob of previous plan
      if (ob.floorSVG) {
        ob.floorSVG.svg.remove()
      } else {
      }
      // load the correct floor plan
      //

      switch (e.innerHTML) {
        case "Fourth Floor":
          ob.floorSVG = svgDraw("./resources/fourth_floor_keating_2019.svg",rooms)
          break
        case "Third Floor":
          ob.floorSVG = svgDraw("./resources/third_floor_keating_2019.svg",rooms)
          break
        case "Second Floor": 
          ob.floorSVG = svgDraw("./resources/second_floor_keating_2019.svg",rooms)
          break
        case "First Floor":
          ob.floorSVG = svgDraw("./resources/first_floor_keating_2019.svg",rooms)
          break
        default:
          console.log("broken room load")
      }
    })
  })
  return ob
}
// video element added to the bottom
window.onload = ()=> {
  let floorsActive = floorNav()
  document.querySelectorAll(".floor")[3].click()
  
}
