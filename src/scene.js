// pop out elements

AFRAME.registerComponent("loading",{
  init:function() {
    this.el.setAttribute("text",{
      value:"Please Wait, Bio5 Scene Loading ...",
      color:"#000000",
    })
  }
})

AFRAME.registerComponent('mlisten',{
  schema:{
    popoutInnerHTML:{type:'string'}
  },
  init:function () {
    let el = this.el
    let innerHTML = this.data.popoutInnerHTML
    this.el.addEventListener('mouseenter',function(e) {
      // play with emissivity
      el.setAttribute("material","color","#5ef7ff")
      //el.setAttribute("material","emissiveIntensity","1")
    })
    this.el.addEventListener("mouseleave",function() {
      //el.setAttribute("material", "emissiveIntensity",'.2')
      el.setAttribute("material","color","white")
    })
    this.el.addEventListener("click",function(){
      // load whatever popout is needed for this part
      let popoutItem = textPopout()
      popoutItem.create(innerHTML)

    })
  }
})

let sceneInfo = {
  "start-rect":{
    image:"./resources/360-assets/DSCN0196.JPG",
    json:""
  },
  "art-one":{
    image:"./resources/360-assets/DSCN0198.JPG",
    json:{
      sky:{rotation:"3.04 0 0",},
      icons:[
        {
          type:"info",
          position:"-12.4 0 -5.1",
          rotation:"0 90 0"
        }
      ]
    }
  },
  "art-two":{
    image:"./resources/360-assets/DSCN0200.JPG",
    json:{
      icons:[
        {
          type:"info",
          position:"2.93554 4.28119 14.79419",
          rotation:"0 -178.74105968460188 0"
        }
      ]
    }
  },
  "art-three":{
    
    image:"./resources/360-assets/DSCN0194.JPG",
    json:{
      icons:[
        {
          type:"info",
          position:"-19.15157 8.99412 -5.62898",
          rotation:"0 73.49 0",
          innerHTML:`
<h1>
  David T Kessler
</h1>

<h2>
  About the Artist
</h2>


<p>
  David Kessler emerged as a photo-realist painter in California in the mid-1970's. Known for taking the visual aberrations of unsuccessful photographs and incorporating them into compositions in his Ruin Slide Series, he departed from other painters in that genre and confirmed his interest in perceptions of abstraction, illusion, and realism. In 2003, this work was selected, from among 20 American painters, to be included in Hyper-realisms USA 1965-1975 at the Strasbourg Museum of Contemporary Art. In more recent series, Mr. Kessler has concentrated on the bravura rendering of photo-realistic scenes in nature. In the early 1980's, he painted abstract dashes of color on Plexiglas, which he set up in front of a landscape to be photographed as source material, thus lending a surreal quality to the realistic scene. 
</p>
<p>
  In the 1990's, he began painting on aluminum and rough-brushing the surface of the raw metal to create compositions that combine painted and non-painted portions of the surface. Mr. Kessler's work is in the collections of many museums in the United States of Europe. 
</p>
<img src="resources/VR Art Tour Assets/David T Kessler/David T Kessler - Cat_s Claw Elegance - Acrylic on Aluminum - 2006 - PC -.png" alt="">
          `
        },
        {
          type:"info",
          position:"8.13123 0.79036 0.39047",
          rotation:"0 -90 0"
        }
      ]
    }
  },
  "art-four":{
    image:"./resources/360-assets/DSCN0194.JPG",
    json:{
      icons:[
        {
          type:"info",
          position:"2.93554 4.28119 14.79419",
          rotation:"0 -178.74105968460188 0"
        },
      ]
    }
  }
}

let generateIcon = (config)=> {
  let ob = {}
  let circle = document.createElement("a-circle")
  circle.setAttribute("position",config.position)
  circle.setAttribute("rotation",config.rotation)
  circle.setAttribute("material","emissive","#9A9A9A")
  circle.setAttribute("material","emissiveIntensity",".3")
  if (config.type == "info") {
    circle.setAttribute("src","./resources/info.png")
  } else {
    // do the movement icon
  }
  // add the event listener
  circle.setAttribute("mlisten",{
    popoutInnerHTML:config.innerHTML
  })
  ob.element = circle
  return ob
}


let basicScene = (room) => {
  let ob = {}
  ob.pointers =[] // this is the points of interest array will hold the objects
  ob.image = {} // structure will be name and path
  // the generic scene creation calls for aframe
  ob.create = ()=> {

    let scene = document.createElement("a-scene")
    scene.setAttribute("cursor","rayOrigin: mouse")
    // add rayOrigin mouse
    console.log("made scene")
    document.body.append(scene)
    let sky = document.createElement("a-sky")
    // for some reason the sky appears tilted
    sky.setAttribute("rotation","-5.3 0 0")
    // this is the connection between the svg icons and the scenes that get loaded
    // room jsons
    // these are the files which contain the scene configuration details 
    let specScene = sceneInfo[room]
    sky.setAttribute("src",specScene.image)
    if (specScene.json.sky) {
      sky.setAttribute("rotation",specScene.json.sky.rotation)
    }
    scene.append(sky)
    let text = document.createElement("a-entity")
    text.setAttribute("loading","")
    text.setAttribute("position","0 .9 -4.1")
    text.setAttribute("scale","8 8 8")
    scene.append(text)

    // add the elements to the scene that are specified in the info
    for (let entityInfo of specScene.json.icons) {
      let icon = generateIcon(entityInfo)
      scene.append(icon.element)
    }
    // add a exit scene icon in the top left to allow for click exit
    let sceneExit = new Image()
    sceneExit.src = "./resources/brand_icons/ua-brand-icons/ua-brand-icons-image-files/SVG/x.svg"
    sceneExit.id = "sceneex"
    sceneExit.onload = ()=>{
      document.body.append(sceneExit)
    }
    sceneExit.addEventListener("click",()=> {
      scene.remove()
      sceneExit.remove()
    })
    

  }
  return ob
}

// 
